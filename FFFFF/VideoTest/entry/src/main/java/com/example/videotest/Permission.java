package com.example.videotest;

import com.example.videotest.slice.PermissionSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.bundle.IBundleManager;
import ohos.security.SystemPermission;

import java.util.Arrays;

public class Permission extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(PermissionSlice.class.getName());

        if(verifySelfPermission(SystemPermission.DISTRIBUTED_DATASYNC) == IBundleManager.PERMISSION_DENIED){
            requestPermissionsFromUser(new String[]{SystemPermission.DISTRIBUTED_DATASYNC}, 0);
        }else{
            Intent intent_ = new Intent();
            IntentParams intentParams = new IntentParams();
            intentParams.setParam("res", 0);
            intent.setParams(intentParams);
            setResult(0, intent_);
            terminateAbility();
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        IntentParams intentParams = new IntentParams();
        if(requestCode == 0 && Arrays.stream(grantResults).sum() == 0){
            intentParams.setParam("res", 0);
        }else{
            intentParams.setParam("res", 1);
        }
        Intent intent = new Intent();
        intent.setParams(intentParams);
        setResult(0, intent);
        terminateAbility();
    }
}
