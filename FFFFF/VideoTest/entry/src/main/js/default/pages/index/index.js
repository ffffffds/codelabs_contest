import prompt from '@system.prompt';
import deviceInfo from '@ohos.deviceInfo';

export default {
    data: {
        videos: ['s1.mp4','s2.mp4','s3.mp4','s4.mp4'],
        right: ['0:1','1:2','2:0'],
        left: ['2:1','1:0','0:2'],
        nowVideo: [0,1,2],
        nowPrepare: [false,false,false],
        lastIdx: 0,
        contents: [{own:false,content:'为鸿蒙打call'},
                   {own:true,content:'真好看啊！！'},
                   {own:false,content:'加一加一，我也觉得'}],
        contentInput: '',
        contentInputBuf: '',
        currentTime: 0,
        start: true,
        deviceType: deviceInfo.deviceType
    },

    onShow(){
        //将评论滚动置底部
        this.$refs.contentContainer.scrollBottom({smooth: true})
    },

    videoPrepare(idx){
        console.warn('idx:' + idx + ' prepared, now is ' + this.lastIdx)
        this.nowPrepare[idx] = true
        if(this.lastIdx == idx) {
            //加载完成后，若为当前播放视频，则播放
            this.$element('video_' + this.lastIdx).setCurrentTime({currenttime:0})
            this.$element('video_' + this.lastIdx).start()
            //首次页面打开，更新播放位置
            if(this.start){
                this.start = false
                this.$element('video_' + this.lastIdx).setCurrentTime({currenttime: this.currentTime})
            }

        }
    },

    swipe(e){
        //停止上一页面视频播放
        this.$element('video_' + this.lastIdx).setCurrentTime({currenttime:0})
        this.$element('video_' + this.lastIdx).stop()

        //获取最新索引
        let idx = e.index

        //更新轮播插件的左右索引
        this.changeIndex(idx)

        //如果准备就绪，则播放
        if(this.nowPrepare[idx]){
            this.$element('video_' + idx).setCurrentTime({currenttime:0})
            this.$element('video_' + idx).start()
        }

        //记录本次索引
        this.lastIdx = idx
    },

    changeIndex(idx){
        let last = this.lastIdx
        let l = this.left.indexOf(last+':'+idx) //判断是否左移
        let r = this.right.indexOf(last+':'+idx) //判断是否右移
        let n = this.nowVideo[idx] //获取当前播放索引
        if(l != -1){ //左移，则更新当前索引的下一位置为当前播放索引+1，循环
            let nIdx = n - 1 < 0 ? this.videos.length - 1 : n - 1
            if(idx == 0){
                if(this.nowVideo[2] != nIdx) this.nowPrepare[2] = false
                this.nowVideo[2] = nIdx
            }else if(idx == 1){
                if(this.nowVideo[0] != nIdx) this.nowPrepare[0] = false
                this.nowVideo[0] = nIdx
            }else{
                if(this.nowVideo[1] != nIdx) this.nowPrepare[1] = false
                this.nowVideo[1] = nIdx
            }
        }else if(r != -1){ //右移，则更新当前索引的上一位置为当前播放索引-1，循环
            let nIdx = n + 1 >= this.videos.length ? 0 : n + 1
            if(idx == 0){
                if(this.nowVideo[1] != nIdx) this.nowPrepare[1] = false
                this.nowVideo[1] = nIdx
            }else if(idx == 1){
                if(this.nowVideo[2] != nIdx) this.nowPrepare[2] = false
                this.nowVideo[2] = nIdx
            }else{
                if(this.nowVideo[0] != nIdx) this.nowPrepare[0] = false
                this.nowVideo[0] = nIdx
            }
        }else{
            console.error('can not deal.')
        }
        console.log(this.nowVideo)

        //有时候修改数组值界面不会刷新，通过该命令令界面更新，也可以使用.push()
        this.nowVideo.unshift()
    },

    contentChange(e){
        //文本框改变，修改输入缓冲
        this.contentInputBuf = e.value
    },

    sendContent(){
        //将输入缓存更新到content
        this.contentInput = this.contentInputBuf

        //判断输入为空
        if(this.contentInput == ''){
            prompt.showToast({message:'请输入内容再发布'})
            return
        }

        //添加评论，可存入数据库...
        this.contents.push({
            own:true,
            content: this.contentInputBuf
        })

        //清空输入框
        this.contentInputBuf = ''
        this.contentInput = ''

        //将评论滚动置底部
        this.$refs.contentContainer.scrollBottom({smooth: true})
    },

    updateTime(idx, e){
        //判断是否是当前播放组件
        if(idx == this.lastIdx){
            this.currentTime = e.currenttime
        }
    },

    async continuation(){
        //动态申请权限
        let res = await FeatureAbility.startAbilityForResult({
            bundleName: 'com.example.videotest',
            abilityName: 'com.example.videotest.Permission',
            deviceType: 1
        })
        let data = JSON.parse(res.data)

        //判断权限结果
        if (data.parameters != undefined && data.parameters.res == 1) {
            prompt.showToast({
                message: '请提供权限'
            })
            //重新播放视频
            this.$element('video_' + this.lastIdx).start()
            return
        }

        // 应用进行迁移
        let result = await FeatureAbility.startAbility({
            bundleName: 'com.example.videotest',
            abilityName: 'com.example.videotest.MainAbility',
            deviceType: 0,
            data: {
                lastIdx: this.lastIdx,
                currentTime: this.currentTime,
                nowVideo: this.nowVideo,
                contents: this.contents
            }
        })
        console.log(JSON.stringify(result))
    },
}
